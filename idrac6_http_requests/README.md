
# Authorization

> - Perform a **POST** request to *https://***fqdn***/data/login* with the following:

### HTTP Headers

| Header Name | Header Value |
|---|---|
| Accept-Language | en-US,en;q=0.8,sv;q=0.6 |
| Accept-Encoding | gzip,deflate,sdch |
| Content-Type | application/x-www-form-urlencoded |

### Body

> **x-www-form-urlencoded**

| Key | Value |
|--|--|
| User | root |
| Password | calvin |

### Response

>Capture the reponse body and extract ST2 from ***root.forwardUrl***

```xml
<root>
    <status>ok</status>
    <authResult>0</authResult>
    <forwardUrl>index.html?ST1=5e27e0194de525531f4bf57647e2993a,ST2=0ba5960e3658e89d4107c1223397ba1f</forwardUrl>
</root>
```

---

# Getting Power State

> - Perform a **POST** request to *https://***fqdn***/data?get=pwState,* with the following:

### HTTP Headers

| Header Name | Header Value |
|---|---|
| Accept-Language | en-US,en;q=0.8,sv;q=0.6 |
| Accept-Encoding | gzip,deflate,sdch |
| ST2 | {Value extracted from above} |

### Body

> No body.

### Response

>Capture the reponse body and extract pwState from ***root.pwState***

```xml
<root>
    <pwState>1</pwState>
    <status>ok</status>
</root>
```

### Power States

| ID | Description |
|--|--|
| 0 | Powered Off |
| 1 | Powered On |

---

# Setting Power State

> - Perform a **POST** request to *https://***fqdn***/data?set=pwState:**{id}*** with the following:

### HTTP Headers

| Header Name | Header Value |
|---|---|
| Accept-Language | en-US,en;q=0.8,sv;q=0.6 |
| Accept-Encoding | gzip,deflate,sdch |
| ST2 | {value extracted from above} |

### Body

> No body

### Power Actions (to be used in **{id}** above)

| ID | Description |
|--|--|
| 0 | Power Off System |
| 1 | Power On System |
| 2 | Power Cycle System (cold boot) |
| 3 | Reset System (warm boot) |
| 4 | NMI (Non-Masking Interrupt)
| 5 | Graceful Shutdown |